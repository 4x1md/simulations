# LTspice Simulations

Simulations created for verifying possible functionality of different circuits and for studying LTspice.

## TRF Radio Receiver

![Circuit](https://gitlab.com/4x1md/simulations/raw/master/images/trf_receiver.gif)

[Source](http://radiosparks.com/schematics.asp?UID=radio+receivers+-+trf)

## RF Preamplifier

RF preamplifier for LW and MW broadcast bands. The preamplifier has input imipedance of 50 Ohm and drives a 50 Ohm load.

![Circuit](https://gitlab.com/4x1md/simulations/raw/master/images/rf_preamp_schematic.png)

![Simulation](https://gitlab.com/4x1md/simulations/raw/master/images/rf_preamp_output.png)

## Multivibrators

![Multivibrator 1](https://gitlab.com/4x1md/simulations/raw/master/images/multivibrator1.png)

![Multivibrator 2](https://gitlab.com/4x1md/simulations/raw/master/images/multivibrator2.png)

## Links

